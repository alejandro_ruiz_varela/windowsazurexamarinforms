﻿using System;
using Xamarin.Forms;
using WindowsAzureXamarinForms.Models;

namespace WindowsAzureXamarinForms.Pages
{
	public class RootPage:ContentPage
	{
		protected Label HeaderTitulo { get; set; }

		protected Label HeaderNombre { get; set; }
		protected Label HeaderDomicilio { get; set; }
		protected Label HeaderCorreo { get; set; }
		protected Label HeaderEdad { get; set; }
		protected Label HeaderSaldo { get; set; }
		protected Entry EntryNombre { get; set; }
		protected Entry EntryDomicilio { get; set; }
		protected Entry EntryCorreo { get; set; }
		protected Entry EntryEdad { get; set; }
		protected Entry EntrySaldo { get; set; }
		protected Button ButtonGuardar { get; set; }
		protected Label HeaderNombreExtraer { get; set; }
		protected Entry EntryNombreExtraer { get; set; }
		protected Button ButtonExtraer { get; set; }
		protected Button ButtonExtraerTodo { get; set; }
		protected ListView ListaRegistros { get; set; }
		protected StackLayout MainStack { get; set; }

		protected void Init()
		{
			HeaderTitulo = new Label(){
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Text = "Xamarin Forms con Windows Azure"
			};

			HeaderNombre = new Label(){
				Text = "Nombre:"
			};

			HeaderDomicilio = new Label(){
				Text = "Domicilio:"
			};

			HeaderCorreo = new Label(){
				Text = "Correo:"
			};

			HeaderEdad = new Label(){
				Text = "Edad:"
			};

			HeaderSaldo = new Label(){
				Text = "Saldo:"
			};

			EntryNombre = new Entry () {
			};

			EntryDomicilio = new Entry () {
			};

			EntryCorreo = new Entry () {
			};

			EntryEdad = new Entry () {
			};

			EntrySaldo = new Entry () {
			};

			ButtonGuardar = new Button () {
				Text = "Guardar en la Nube"
			};

			ButtonGuardar.Clicked += async (sender, e) => {
				Datos data = new Datos();
				data.Correo = EntryCorreo.Text;
				data.Domicilio = EntryDomicilio.Text;
				data.Edad = Convert.ToInt32(EntryEdad.Text);
				data.Nombre = EntryNombre.Text;
				data.Saldo = Convert.ToDouble(EntrySaldo.Text);
				var result = await App.AzureManager.GuardarDatos(data);
				await DisplayAlert("Xamarin Forms Microsoft Azure", result ? "Guardado Correctamente" : "Error al registrar", "Ok");
			};

			HeaderNombreExtraer = new Label(){
				Text = "Nombre:"
			};

			EntryNombreExtraer = new Entry () {
			};

			ButtonExtraer = new Button () {
				Text = "Extraer de la Nube"
			};
			ButtonExtraer.Clicked += async (sender, e) => {
				var result = await App.AzureManager.Registro(EntryNombreExtraer.Text);
				if(result!=null){
					EntryCorreo.Text = result.Correo;
					EntryDomicilio.Text = result.Domicilio;
					EntryEdad.Text = result.Edad.ToString();
					EntryNombre.Text = result.Nombre;
					EntrySaldo.Text = result.Saldo.ToString();
				}
			};

			ButtonExtraerTodo = new Button () {
				Text = "Extraer todos los datos de la Nube"
			};

			ButtonExtraerTodo.Clicked += async (sender, e) => {
				var result = await App.AzureManager.Registros();
				ListaRegistros.ItemsSource = result;
			};

			ListaRegistros = new ListView () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 500
			};

			var itemtemplate = new DataTemplate (typeof(TextCell));
			itemtemplate.SetBinding (TextCell.TextProperty, "Nombre");
			itemtemplate.SetBinding (TextCell.DetailProperty, "Domicilio");

			ListaRegistros.ItemTemplate = itemtemplate;

			ListaRegistros.ItemTapped += async (sender, e) => {
				var item = (e.Item as Datos);
				if(item!=null){
					await DisplayAlert("Xamarin Forms Microsoft Azure",
						"Id: "+item.Id +
						"\nNombre: " + item.Nombre +
						"\nDomicilio: " + item.Domicilio +
						"\nCorreo: "+ item.Correo +
						"\nEdad: "+ item.Edad +
						"\nSaldo: " + item.Saldo,
						"Ok");
				}
			};

			MainStack = new StackLayout () {
				Padding = new Thickness(10,30,10,0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			MainStack.Children.Add (HeaderTitulo);
			MainStack.Children.Add (HeaderNombre);
			MainStack.Children.Add (EntryNombre);
			MainStack.Children.Add (HeaderDomicilio);
			MainStack.Children.Add (EntryDomicilio);
			MainStack.Children.Add (HeaderCorreo);
			MainStack.Children.Add (EntryCorreo);
			MainStack.Children.Add (HeaderEdad);
			MainStack.Children.Add (EntryEdad);
			MainStack.Children.Add (HeaderSaldo);
			MainStack.Children.Add (EntrySaldo);

			MainStack.Children.Add (ButtonGuardar);

			MainStack.Children.Add (HeaderNombreExtraer);
			MainStack.Children.Add (EntryNombreExtraer);

			MainStack.Children.Add (ButtonExtraer);

			MainStack.Children.Add (ButtonExtraerTodo);

			MainStack.Children.Add (ListaRegistros);
		}

		public RootPage ()
		{
			Init ();

			this.Content = new ScrollView () {
				IsClippedToBounds = true,
				Content = MainStack
			};
		}
	}
}

