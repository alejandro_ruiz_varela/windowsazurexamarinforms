﻿using System;
using Xamarin.Forms;
using WindowsAzureXamarinForms.Pages;
using WindowsAzureXamarinForms.Interfaces;
using WindowsAzureXamarinForms.Managers;

namespace WindowsAzureXamarinForms
{
	public class App:Application
	{
		public static IAzureManager AzureManager { get; set; } 

		public App()
		{
			if (string.IsNullOrWhiteSpace (AzureConfigurationStrings.AplicationUrl))
				throw new Exception ("Aplication Url must be set on AzureConfigurationStrings.AplicationUrl");
			
			if(string.IsNullOrWhiteSpace(AzureConfigurationStrings.AplicationKey))
				throw new Exception ("Aplication Key must be set on AzureConfigurationStrings.AplicationKey");
			
			AzureManager = new AzureManager ();
			AzureManager.Init ();
			MainPage = new NavigationPage (new RootPage ());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

