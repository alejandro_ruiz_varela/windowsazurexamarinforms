﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using WindowsAzureXamarinForms.Models;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace WindowsAzureXamarinForms.Interfaces
{
	public interface IAzureManager
	{
		void Init();
		Task<bool> GuardarDatos (Datos data);
		Task<Datos> Registro (string nombre);
		Task<ObservableCollection<Datos>> Registros ();
	}
}

