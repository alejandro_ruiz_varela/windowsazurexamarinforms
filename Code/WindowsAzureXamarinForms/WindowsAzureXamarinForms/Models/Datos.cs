﻿using System;

namespace WindowsAzureXamarinForms.Models
{
	public class Datos
	{
		public string Id { get; set; }
		public string Nombre { get; set; }
		public string Domicilio { get; set; }
		public string Correo { get; set; }
		public int Edad { get; set; }
		public double Saldo { get; set; }
	}
}

