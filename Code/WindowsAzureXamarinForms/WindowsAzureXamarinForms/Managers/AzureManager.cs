﻿using System;
using WindowsAzureXamarinForms.Interfaces;
using WindowsAzureXamarinForms.Interfaces;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;
using WindowsAzureXamarinForms.Models;
using System.Linq;
using System.Collections.ObjectModel;

namespace WindowsAzureXamarinForms.Managers
{ 
	public class AzureManager:IAzureManager
	{
		#region IAzureManager implementation

		public void Init ()
		{
			CurrentPlatform.Init();
		}

		public Task<bool> GuardarDatos (Datos data)
		{
			return Task.Run(()=>{
				try
				{
					MobileService.GetTable<Datos>().InsertAsync(data);
					return true;
				}
				catch(Exception ex) {
					Console.WriteLine(ex.Message);
					return false;
				}
			});
		}

		public Task<Datos> Registro (string nombre)
		{
			return Task.Run(()=>{
				try
				{
					var retorno = MobileService.GetTable<Datos>().Where( item => item.Nombre == nombre).ToListAsync().Result;
					return retorno.FirstOrDefault();
				}
				catch(Exception ex) {
					Console.WriteLine(ex.Message);
					return null;
				}
			});
		}

		public Task<ObservableCollection<Datos>> Registros ()
		{
			return Task.Run(()=>{
				try
				{
					var retorno = MobileService.GetTable<Datos>().Select( item => item).ToListAsync().Result;
					return new ObservableCollection<Datos>(retorno);
				}
				catch(Exception ex) {
					Console.WriteLine(ex.Message);
					return null;
				}
			});
		}

		#endregion

		public MobileServiceClient MobileService = new MobileServiceClient(
			AzureConfigurationStrings.AplicationUrl,
			AzureConfigurationStrings.AplicationKey
		);

		public AzureManager ()
		{
		}
	}
}

